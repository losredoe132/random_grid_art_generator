import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
import os
import numpy as np
import imageio
import logging
from typing import List

from images import ImageLoader
from canvas import Canvas
from post_processing import PostProcessor


class Frame:
    def __init__(self, width, height, fps=4, export_dir: str = "imgs"):
        self.width = width
        self.height = height
        self.canvas = Canvas(width, height, 0, 0, "F")
        self.img = np.zeros((width, height, 3))
        self.fps = fps
        self.export_dir = export_dir
        if not os.path.isdir(self.export_dir):
            logging.info(f"generating folder: {self.export_dir} because it does not exist")
            os.makedirs(self.export_dir)

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def add_canvas(self, canvas):
        self.canvas = canvas

    def render(
        self,
        il: ImageLoader,
        export_gif: bool = False,
        show_img: bool = True,
        save_img: bool = False,
    ):
        # recursive call
        images = []
        patches = []

        pp = PostProcessor()

        def recursive_render(c: Canvas, img: np.ndarray):
            images.append(pp.postprocess(img))
            patches.append(Rectangle([c.pos_abs_x, c.pos_abs_y], c.width, c.height))

            if c.get_n_available() != 0 or True:
                x = c.pos_abs_x
                y = c.pos_abs_y

                img_patch = il.get_suitable_image([c.width, c.height])
                img[x : x + c.width, y : y + c.height] = img_patch

            if c.sub_canvas_0 is not None:
                recursive_render(
                    c.sub_canvas_0,
                    img,
                )
            if c.sub_canvas_1 is not None:
                recursive_render(
                    c.sub_canvas_1,
                    img,
                )

            return

        recursive_render(self.canvas, self.img)

        pc = PatchCollection(patches, alpha=0.3, facecolor=None, edgecolor="k")
        fig, ax = plt.subplots(1, 2, figsize=(8, 4))
        ax[0].set_xlim([0, self.width])
        ax[0].set_ylim([0, self.height])
        ax[0].add_collection(pc)

        ax[1].imshow(
            np.transpose(images[-1].astype(np.uint8), (1, 0, 2)),
            origin="lower",
        )

        if save_img:
            path = os.path.join(self.export_dir, "img.png")
            logging.info(f"saving image @ {path}")
            from PIL import Image

            im = np.transpose(Image.fromarray(images[-1]), (1, 0, 2))
            im.save(path)

        if show_img:
            plt.show()

        if export_gif:
            logging.info("exporting gif")
            imageio.mimsave(
                os.path.join(self.export_dir, "anim.gif"),
                images,
                fps=self.fps,
            )

    def get_nodes(self, mode="all"):
        def recursive_find_leafs(c: Canvas, nodes: List[Canvas]):
            condition = {
                "all": True,
                "leaf": c.get_n_available() == 2,
                "less_than_2": c.get_n_available() > 0,
            }[mode]

            if condition:
                nodes.append(c)

            if c.sub_canvas_0 is not None:
                recursive_find_leafs(c.sub_canvas_0, nodes)
            if c.sub_canvas_1 is not None:
                recursive_find_leafs(c.sub_canvas_1, nodes)

            return

        if mode not in ["all", "leaf", "less_than_2"]:
            raise NotImplementedError(f"condition {mode} not implemented")

        nodes = []
        recursive_find_leafs(self.canvas, nodes)
        return nodes
