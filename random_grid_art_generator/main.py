import numpy as np
import random
import logging
import argparse

from frame import Frame
from canvas import Canvas
from images import DirImageLoader, ConstantColorLoader, GoogleImageLoader

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(description="main arg parser")
    parser.add_argument("--n_splits", default=10, type=int, help="maximum number of splits")
    parser.add_argument("--seed", default=None, type=int, help="random seed")
    parser.add_argument(
        "--mode",
        default="const",
        type=str,
        help="""mode, available: const (homogenous fill with random color),
     dir (images in a specified directory, argument --source_img_dir),
      google (google image search with keyword specified by --keyword)""",
    )
    parser.add_argument(
        "--source_img_dir", default="pics", type=str, help="directory containing the image used by mode 'dir'"
    )
    parser.add_argument("--keyword", default="dark forest", type=str, help="keyword used by mode 'google'")
    parser.add_argument("--width", default=1024, type=int, help="width of the frame in px")
    parser.add_argument("--height", default=1024, type=int, help="height of the frame in px")
    parser.add_argument("--fps", default=4, type=int, help="fps of GIF export")
    parser.add_argument("--save_img", "-i", action="store_true", help=" image will be saved")
    parser.add_argument("--save_gif", "-g", action="store_true", help="If True, a GIF will be saved")

    args = parser.parse_args()
    for key, val in args.__dict__.items():
        print(f"{key} : {val}")
    seed = args.seed
    if not seed:
        seed = random.randint(0, 10000)

    logging.info(f"using seed: {seed}")
    np.random.seed(seed)
    random.seed(seed)

    F = Frame(args.width, args.height, fps=args.fps)

    n_splits = args.n_splits
    for i in range(n_splits):
        # select random leaf node
        cs = F.get_nodes(mode="less_than_2")
        area = [c.width * c.height for c in cs]
        c: Canvas = random.choices(cs, weights=area, k=1)[0]

        # check if selected canvas splitable
        if c.width < 5 or c.height < 5:
            logging.warning("canvas to small to split")
            continue

        c.insert_random_canvas()

    if args.mode == "const":
        il = ConstantColorLoader()
    elif args.mode == "dir":
        il = DirImageLoader(args.source_img_dir)
    elif args.mode == "google":
        il = GoogleImageLoader(keyword=args.keyword)

    F.render(il=il, export_gif=args.save_gif, save_img=args.save_img)
