import logging
import numpy as np
import random


class CanvasIsFullException(Exception):
    pass


class Canvas:
    def __init__(self, width, height, pos_abs_x, pos_abs_y, canvas_id=""):
        self.id = canvas_id
        self.width = width
        self.height = height
        self.pos_abs_x = pos_abs_x
        self.pos_abs_y = pos_abs_y
        self.sub_canvas_0 = None
        self.sub_canvas_1 = None

        wh_ratio = self.width / self.height
        self.canvas_split_dir = random.choices([0, 1], weights=[1 / wh_ratio, wh_ratio], k=1)[0]
        self.canvas_split_pos = self._get_random_split_pos()
        logging.debug(f"Canvas with id {self.id} generated.")

    def _get_random_split_pos(self):
        if self.canvas_split_dir == 0:
            # horizontally
            return int(np.random.uniform(1, self.height))
        else:
            # vertically
            return int(np.random.uniform(1, self.width))

    def get_n_available(self):
        return sum([1 for var in [self.sub_canvas_0, self.sub_canvas_1] if var is None])

    def get_size_pos_of_new_canvas(self, idx):
        if idx == 0:
            if self.canvas_split_dir == 0:
                new_width = self.width
                new_height = self.canvas_split_pos
                new_x = 0
                new_y = 0
            if self.canvas_split_dir == 1:
                new_width = self.canvas_split_pos
                new_height = self.height
                new_x = 0
                new_y = 0

        if idx == 1:
            if self.canvas_split_dir == 0:
                new_width = self.width
                new_height = self.height - self.canvas_split_pos
                new_x = 0
                new_y = self.canvas_split_pos
            if self.canvas_split_dir == 1:
                new_width = self.width - self.canvas_split_pos
                new_height = self.height
                new_x = self.canvas_split_pos
                new_y = 0

        return {
            "size": {"width": new_width, "height": new_height},
            "pos": {"x": new_x, "y": new_y},
        }

    def insert_random_canvas(self):
        if self.get_n_available() == 0:
            raise CanvasIsFullException("There is no empty place in this Canvas")

        available = []
        if self.sub_canvas_0 is None:
            available.append(0)
        if self.sub_canvas_1 is None:
            available.append(1)
        selection = random.choice(available)

        if selection == 0:
            result = self.get_size_pos_of_new_canvas(0)
            self.sub_canvas_0 = Canvas(
                result["size"]["width"],
                result["size"]["height"],
                self.pos_abs_x + result["pos"]["x"],
                self.pos_abs_y + result["pos"]["y"],
                self.id + "0",
            )

        if selection == 1:
            result = self.get_size_pos_of_new_canvas(1)
            self.sub_canvas_1 = Canvas(
                result["size"]["width"],
                result["size"]["height"],
                self.pos_abs_x + result["pos"]["x"],
                self.pos_abs_y + result["pos"]["y"],
                self.id + "1",
            )

    def __repr__(self) -> str:
        split_dir = ["horizontaly", "verticaly"][self.canvas_split_dir]
        str_meta = f"Canvas {self.width, self.height}"
        str_splt = f"splitted {split_dir} @ {self.canvas_split_pos} containing {2-self.get_n_available()}"

        return str_meta + str_splt


if __name__ == "__main__":
    c = Canvas(100, 100, 0, 0)
    c.insert_random_canvas()
    c.insert_random_canvas()
    c.sub_canvas_0.insert_random_canvas()
    c.sub_canvas_1.insert_random_canvas()
    print(c)
