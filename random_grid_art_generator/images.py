import numpy as np
import glob
import os
from PIL import Image
import logging
from dotenv import dotenv_values
from serpapi import GoogleSearch
import skimage


class NoSuitableImageAvailableException(Exception):
    def __init__(self, size):
        self.size = size
        self.message = f"There is no image wich fits size: {size}"
        super().__init__(self.message)


class ImageLoader:
    def __init__(self) -> None:
        pass

    def random_crop(self, img, size) -> np.ndarray:
        difference = img.shape[:2] - np.array(size)
        start_x = np.random.randint(0, difference[0])
        start_y = np.random.randint(0, difference[1])
        return img[start_x : start_x + size[0], start_y : start_y + size[1]]


class DirImageLoader(ImageLoader):
    def __init__(self, root) -> None:
        self.imgs_root = root
        self.imgs_paths = glob.glob(os.path.join(self.imgs_root, "*.jpg"), recursive=False)

    def get_suitable_image(self, size):
        np.random.shuffle(self.imgs_paths)
        for img_path in self.imgs_paths:
            img = Image.open(img_path)
            # check if image is large enough
            if img.height >= size[0] and img.width >= size[1]:
                logging.debug(f"picking img: {img_path}")
                return self.random_crop(np.array(img), size)
            if img.height >= size[1] and img.width >= size[0]:
                logging.debug(f"picking img: {img_path}")
                return self.random_crop(np.transpose(np.array(img), (1, 0, 2)), size)
            logging.debug(f"img {img_path} ({img.width}x{img.height}) too small for ({size})")
        raise NoSuitableImageAvailableException(size)


class GoogleImageLoader(ImageLoader):
    def __init__(self, keyword) -> None:
        self.config = dotenv_values(".env")
        GoogleSearch.SERP_API_KEY = dotenv_values(".env")["serpapi_key"]
        self.search = GoogleSearch({"q": keyword, "tbm": "isch", "num": 10})
        self.results = self.search.get_dict()["images_results"]
        self.n_results = len(self.results)
        logging.info(f"found {self.n_results} matching images")

    def get_suitable_image(self, size):
        results_order = np.arange(self.n_results)
        np.random.shuffle(results_order)
        for image_result_idx in results_order:
            url = self.results[image_result_idx]["original"]
            try:
                img: np.ndarray = skimage.io.imread(url)
                if img.shape[2] != 3:
                    continue
                if img.shape[0] >= size[0] and img.shape[1] >= size[1]:
                    logging.info(f"picking img: {url}")
                    return self.random_crop(np.array(img), size)
                if img.shape[0] >= size[1] and img.shape[1] >= size[0]:
                    logging.info(f"picking img: {url}")
                    return self.random_crop(np.transpose(np.array(img), (1, 0, 2)), size)
                logging.debug(f"img {url} ({img.shape[0]}x{img.shape[1]}) too small for ({size})")
            except Exception as e:
                logging.debug(e)


class ConstantColorLoader(ImageLoader):
    def get_colored_plane_noisy(self, width, height):
        base = np.ones((width, height, 3)) * self.get_color()
        noise = np.random.randn(width, height, 3) * 0.4
        return np.clip(base + noise, 0, 1)

    def get_colored_plane(self, width, height):
        color = self.get_color()
        return np.ones((width, height, 3)) * color

    def get_color(self):
        return np.random.uniform(0, 255, 3)

    def get_suitable_image(self, size):
        return self.get_colored_plane(size[0], size[1]).astype(np.uint8)


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    gil = GoogleImageLoader("tree")
    fig, axs = plt.subplots(1, 4, figsize=(8, 3))

    for i, size in enumerate([(100, 100), (1024, 1024), (300, 599), (1200, 2000)]):
        img = gil.get_suitable_image(size)
        axs[i].imshow(img)

    plt.show()
