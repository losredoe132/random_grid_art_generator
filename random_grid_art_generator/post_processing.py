import numpy as np


class PostProcessor:
    def __init__(self) -> None:
        self.noise_amplitude = 50

    def cast_uint8(self, img) -> np.ndarray:
        return np.array(img).astype(np.uint8)

    def noise(self, img) -> np.ndarray:
        noise = np.random.uniform(-self.noise_amplitude, self.noise_amplitude, (img.shape[0], img.shape[1], 1))
        noise = np.repeat(noise, 3, axis=2)
        return img + noise

    def clip(self, img) -> np.ndarray:
        return np.clip(img, 0, 255).astype(np.uint8)

    def postprocess(self, img) -> np.ndarray:
        img = self.cast_uint8(img)
        img = self.noise(img)
        img = self.clip(img)
        return img


if __name__ == "__main__":
    from images import ConstantColorImageLoader
    import matplotlib.pyplot as plt

    ip = ConstantColorImageLoader()
    pp = PostProcessor()
    img = ip.get_suitable_image((500, 450))
    img_pp = pp.postprocess(img)
    fig, axs = plt.subplots(1, 2, figsize=(8, 4))
    axs[0].imshow(img, vmin=0, vmax=255)
    axs[1].imshow(img_pp, vmin=0, vmax=255)
    plt.show()
