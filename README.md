
# Random Grid Art Generator
A generator for random grid art inspired by Piet Mondrian and others.
Example: 
![Animated Drawing](example_img.png)

## Prerequisites
Python >= 3.6 (developed in 3.9)
Requirements are listed in requirements.txt

## Installation
After `git clone` of this repo, run:

```
# navigate into the project folder
cd random_grid_art_generator

# initalize a virtual python environment (venv)
python3 -m venv .venv

# activate venv
source .venv/bin/activate

# install requirements
pip install -r requirements.txt

# install the package itself in -e(ditbale)mode
pip install -e .
```

Create a folder named `pics`, download some images and copy them into the folder. Than run

```
# run generator
python3 random_grid_art_generator/main.py

```

## TODOs

- [x] Argparser
- [ ] User Google Images API to fetch images by keyword
- [ ] Parametrize Random Generators and make the accessible through CLI
  

# Configuration
Configuration is done by CLI arguments. For full documentation, run `python random_grid_art_generator/main.py -h`.

Example for fixing the random seed to 30 and exporting image and gif. 

`python random_grid_art_generator/main.py -s 30 -i -g ` 

To use mode 'google' an authentification key from serpapi is required as environmental variable or inside a `.env`-file with the key *serpapi_key*.

# Contributors
Michel Wörner 
Finn Jonas Peper
